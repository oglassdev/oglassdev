# Hey there! 👋 I'm oglass
Welcome to my GitLab profile! I'm a passionate high school software developer who is learning Rust, Kotlin, and a couple other technologies.

## About Me ℹ️
- 💻 Minecraft Developer (Owner @ Bladehunt)
- 🎥 Small YouTuber (~300 subscribers)
- 🌱 I’m currently learning more about Rust

## Socials 🌐
- [YouTube](https://youtube.com/oglass)
- [GitHub](https://github.com/oglassdev)

## Tech Stack 🛠️
- Kotlin
- Rust
- Docker
- Postgres
- Redis (KeyDB)

<!--
## GitHub Stats 📈

### Featured Repositories 🚀
-->